package avb.gcode.opengl11.nativeLib;

import java.nio.ByteBuffer;

/**
 * The Class NativeLib provides native C functions for memory allocation.
 * The C functions are built with the native development kit.
 * @author Alexander von Birgelen
 */
public class NativeLib {

	  static {
		  System.loadLibrary("ndk_content");
	  }

	/**
  	 * Allocate native buffer.
  	 *
  	 * @param size the size
  	 * @return the byte buffer
  	 */
  	public native ByteBuffer allocNativeBuffer( long size );
	  
	/**
  	 * Free native buffer.
  	 *
  	 * @param buffer the buffer
  	 */
  	public native void freeNativeBuffer(ByteBuffer buffer);

}
