package avb.gcode.opengl11;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import android.opengl.GLU;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;
import avb.gcode.opengl11.objects.Dimension;
import avb.gcode.opengl11.objects.Layer3D;
import avb.gcode.opengl11.objects.NumberXY;
import avb.gcode.opengl11.objects.NumberXZ;
import avb.gcode.process.DataStorage;

/**
 * The Class OpenGLRenderer renders all needed OpenGL data.
 * @author Alexander von Birgelen
 */
public class OpenGLRenderer implements Renderer {

	/** The Constant TAG. */
	private static final String TAG = "OpenGLRenderer"; //For debug only
	
	/** The gcode. */
	private Layer3D[] gcode;
	
	/** The Object dimensions. */
	private Dimension dimension;
	
	/** The minimum x label. */
	private NumberXY label_minx;
	
	/** The maximum x label. */
	private NumberXY label_maxx;
	
	/** The minimum y label. */
	private NumberXY label_miny;
	
	/** The maximum y label. */
	private NumberXY label_maxy;
	
	/** The minimum z label. */
	private NumberXZ label_minz;
	
	/** The maximum z label. */
	private NumberXZ label_maxz;
	
	/** The layers. */
	private int layers;
	
	/** The angle x. */
	public float angleX;
    
    /** The angle y. */
    public float angleY;
    
    /** The scale factor. */
    public float scaleFactor;
    
    /** The translate x. */
    public float translateX;
    
    /** The translate y. */
    public float translateY;
    
    /** The to layer. */
    private static int toLayer;
    
    /** default view. */
    private float default_view = -200.0f;
    
    /** The current model view matrix. */
    private float[] currentModelViewMatrix = new float[16];
    
	/**
	 * Instantiates a new open gl renderer.
	 *
	 * @throws OutOfMemoryError the out of memory error
	 */
	public OpenGLRenderer() throws OutOfMemoryError {
		//retrieve data
		layers = DataStorage.getLayers().size();
		
		float minx = DataStorage.getMinimalX();
		float maxx = DataStorage.getMaximalX();
		float miny = DataStorage.getMinimalY();
		float maxy = DataStorage.getMaximalY();
		float minz = DataStorage.getMinimalZ();
		float maxz = DataStorage.getMaximalZ();
		
		//set default view so you can see the complete object without zooming
		default_view = (-3.5f) * Math.max(maxx - minx, maxy - miny);
		
		try {
			
			//create
			dimension = new Dimension();
			
			//this creates the dimension data to the axis. The enhanced machine controller software displays data in a similar way
			label_minx = new NumberXY
								(
								Float.toString(minx), 
								minx + NumberXY.LETTER_SPACING, 
								miny - NumberXY.LETTER_HEIGHT 
									 - Dimension.DIMENSION_CONSTANT,
								minz
								);
			label_maxx = new NumberXY
								(
								Float.toString(maxx), 
								maxx + NumberXY.LETTER_SPACING, 
								miny - NumberXY.LETTER_HEIGHT 
									 - Dimension.DIMENSION_CONSTANT, 
								minz
								);
			
			label_miny = new NumberXY
								(
								Float.toString(miny), 
								maxx + Dimension.DIMENSION_CONSTANT 
									 + NumberXY.LETTER_SPACING, 
								miny, 
								minz
								);
			label_maxy = new NumberXY
								(
								Float.toString(maxy), 
								maxx + Dimension.DIMENSION_CONSTANT 
									 + NumberXY.LETTER_SPACING, 
								maxy, 
								minz
								);
			
			label_minz = new NumberXZ
								(
								Float.toString(minz), 
								maxx + Dimension.DIMENSION_CONSTANT 
									 + NumberXZ.LETTER_SPACING, 
								miny, 
								minz - NumberXY.LETTER_HEIGHT 
									 - Dimension.DIMENSION_CONSTANT
								);
			label_maxz = new NumberXZ
								(
								Float.toString(maxz), 
								maxx + Dimension.DIMENSION_CONSTANT 
									 + NumberXZ.LETTER_SPACING, 
								miny, 
								maxz
								);
			
			gcode = new Layer3D[layers];
			setToLayer(layers);
			
			//Init layer
			for(int i=0;i<layers;i++) {
					gcode[i] = new Layer3D(i);
			}
			
		}catch(OutOfMemoryError e) {
			
			for(int i=0;i<layers;i++) {
				gcode[i] = null;
			}
			System.gc();
			throw e;
		}
		
	}
	
	/**
	 * Sets the layer.
	 *
	 * @param layer the new layer
	 */
	public void setLayer(int layer) {
		setToLayer(layer);
		Log.d(TAG, "TEST: "+layer);
	}

	/**
	 * Set OpenGL settings when creating surface.
	 * @see android.opengl.GLSurfaceView.Renderer
	 * #onSurfaceCreated(javax.microedition.khronos.opengles.GL10, 
	 * javax.microedition.khronos.egl.EGLConfig)
	 */
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		
		// Set the background color to black ( rgba ).
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
		// Enable Smooth Shading, (not important)
		gl.glShadeModel(GL10.GL_SMOOTH);
		// Depth buffer setup.
		gl.glClearDepthf(1.0f);
		// Enables depth testing.
		gl.glEnable(GL10.GL_DEPTH_TEST);
		// The type of depth testing to do.
		gl.glDepthFunc(GL10.GL_LEQUAL);
		// perspective calculations.
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);	
		
	}


	/**
	 * Draw the items on the surface.
	 * @see android.opengl.GLSurfaceView.Renderer
	 * #onDrawFrame(javax.microedition.khronos.opengles.GL10)
	 */
	public void onDrawFrame(GL10 gl10) {
		
		GL11 gl = (GL11) gl10;
		
		// Clears the screen and depth buffer.
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		// Replace the current matrix with the identity matrix
		gl.glLoadIdentity();
		
		gl.glTranslatef(0, 0, default_view); //THIS IS THE DEFAULT VIEW!!!!!
		//then apply the scaling and translation
		gl.glTranslatef(translateX, translateY, scaleFactor);
		
		//Rotate object using matrices. THIS STILL SUFFERS FROM GIMBAL LOCK.
		gl.glGetFloatv(GL11.GL_MODELVIEW_MATRIX, currentModelViewMatrix,0);
		gl.glRotatef(angleX, currentModelViewMatrix[1], 
					 currentModelViewMatrix[5], currentModelViewMatrix[9]);
		gl.glGetFloatv(GL11.GL_MODELVIEW_MATRIX, currentModelViewMatrix,0);
		gl.glRotatef(angleY, currentModelViewMatrix[0],
					 currentModelViewMatrix[4], currentModelViewMatrix[8]);
		
		// Draw the objects
        for(int i=0;i<getToLayer();i++) {
			gcode[i].draw(gl);
		}
        
        //Draw Object Dimensions
        dimension.draw(gl);
        label_minx.draw(gl);
        label_maxx.draw(gl);
        label_miny.draw(gl);
        label_maxy.draw(gl);
        label_minz.draw(gl);
        label_maxz.draw(gl);

	}


	/**
	 * Called when surface changed.
	 * @see android.opengl.GLSurfaceView.Renderer
	 * #onSurfaceChanged(javax.microedition.khronos.opengles.GL10, int, int)
	 */
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		
		// Sets the current view port to the new size.
		gl.glViewport(0, 0, width, height);
		// Select the projection matrix
		gl.glMatrixMode(GL10.GL_PROJECTION);
		// Reset the projection matrix
		gl.glLoadIdentity();
		// Calculate the aspect ratio of the window
		GLU.gluPerspective(gl, 45.0f, (float)width/(float)height, 0.1f, 
						   1000.0f);	
		// Select the modelview matrix
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		// Reset the modelview matrix
		gl.glLoadIdentity();
		
	}

	/**
	 * Gets the to layer.
	 *
	 * @return the to layer
	 */
	public int getToLayer() {
		return toLayer;
	}

	/**
	 * Sets the to layer.
	 *
	 * @param toLayer the new to layer
	 */
	public void setToLayer(int toLayer) {
		OpenGLRenderer.toLayer = toLayer;
	}
	
	
}
