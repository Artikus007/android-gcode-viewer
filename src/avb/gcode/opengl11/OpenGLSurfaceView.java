package avb.gcode.opengl11;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;

/**
 * The Class OpenGLSurfaceView is the view that contains the OpenGL content.
 * @author Alexander von Birgelen
 */
class OpenGLSurfaceView extends GLSurfaceView {
	
	/** The TAG for log creation. */
	private static final String TAG = "MotionHandler11"; //For debug only
	
    /** The touch scale factor. */
    private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
    
    /** The maximum zoom factor. */
    private final float MAX_ZOOM_FACTOR = 25.0f;
    
    /** The distance between fingers to activate zoom mode. */
    private final float ZOOM_ACTIVATE_DISTANCE = 140f;
    
    /** The finger count. */
    private int fingers = 0;
    
    /** The previous x. */
    private float previousX;
    
    /** The previous y. */
    private float previousY;
    
    /** The angle x. */
    private float angleX;
    
    /** The angle y. */
    private float angleY;
    
    /** The scale factor. */
    private float scaleFactor = 1.0f;
    
    /** The old distance. */
    private float oldDistance;
    
    /** The new distance. */
    private float newDistance;
    
    /** Zoom active indicator. */
    private boolean zoom = false;
    
    /** Rotate juming bugix */
	private boolean block = false;
    
	/** The renderer. */
	public OpenGLRenderer renderer;

    /**
     * Instantiates a new open gl surface view.
     *
     * @param context the context
     * @throws OutOfMemoryError the out of memory error
     */
    public OpenGLSurfaceView(Context context) throws OutOfMemoryError {
        super(context);
        
        //init the opengl renderer
        try {
        	renderer = new OpenGLRenderer();
        }catch(OutOfMemoryError e) {
        	throw e;
        }
 		setRenderer(renderer);
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }
    
    /**
     * Instantiates a new open gl surface view.
     *
     * @param context the context
     * @param attrs the attributes
     * @throws OutOfMemoryError the out of memory error
     * @throws Exception every other exception
     */
    public OpenGLSurfaceView(Context context, AttributeSet attrs) 
    		throws OutOfMemoryError, Exception
	{
		super(context, attrs);	

		try {
			renderer = new OpenGLRenderer();
			setRenderer(renderer);
			setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
		} catch (OutOfMemoryError e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
	}

    /**
     *  Implements multitouch features.
     *  @param e The motion event
     *  @return True or false when Event handled or not.
     *  @see android.view.View#onTouchEvent(android.view.MotionEvent)
     */
    @Override 
    public boolean onTouchEvent(MotionEvent e) {
    	
    		//Get x and y values
    		float x = e.getX();
	        float y = e.getY();
	        
	        switch (e.getAction() & MotionEvent.ACTION_MASK) {
		        case MotionEvent.ACTION_MOVE:
		        	
		        	float dx = x - previousX;
		            float dy = y - previousY;
		        	
		            if (!block) {
			        	if(fingers == 1) {
			        		//ROTATION
				            calculateRotation(dx,dy);
			        	}
			            if(fingers == 2) {
			            	//TRANSLATION OR ZOOM
			            	if(!zoom) {
				            	renderer.translateX += (dx * TOUCH_SCALE_FACTOR)
				            							*0.3f;
				            	renderer.translateY += (dy * TOUCH_SCALE_FACTOR)
				            							*-0.3f;
			            	}else{
			            		newDistance = getFingerDistance(e);
			            		float factor = newDistance/oldDistance;
			            		if(factor > MAX_ZOOM_FACTOR) {
			            			factor = MAX_ZOOM_FACTOR;
			            		}
			            		scaleFactor *= factor;
			            		renderer.scaleFactor = scaleFactor;
			            		oldDistance = newDistance;
			            	}
			            }
		            }
		            //Only needed when using rendermode when dirty
		            requestRender();
		            break;
		        case MotionEvent.ACTION_DOWN:
		        	fingers = 1;	//ROTATE
		        	break;
		        case MotionEvent.ACTION_UP:
		        	fingers = 0;
		        	block = false;
		        	break;
		        case MotionEvent.ACTION_POINTER_DOWN:
		        	fingers = 2;	//MOVE
		        	oldDistance = getFingerDistance(e); //set old distance
		        	if(oldDistance > ZOOM_ACTIVATE_DISTANCE) {
		        		zoom = true;
		        	}else{
		        		zoom = false;
		        	}
		        	break;
		        case MotionEvent.ACTION_POINTER_UP:
		        	fingers = 1;
		        	block = true;
		        	break;
		        	
	        }
	        previousX = x;
	        previousY = y;
        return true;
    }
    
    /**
     * Gets the distance between the fingers.
     *
     * @param event the event
     * @return the finger distance
     */
    private float getFingerDistance(MotionEvent event) {
    	   float x = event.getX(0) - event.getX(1);
    	   float y = event.getY(0) - event.getY(1);
    	   return FloatMath.sqrt(x * x + y * y);
    }
    
    /**
     * Calculate rotation.
     *
     * @param dx the dx
     * @param dy the dy
     */
    private void calculateRotation(float dx, float dy) {
    	
    	angleX += dx * TOUCH_SCALE_FACTOR;
        angleY += dy * TOUCH_SCALE_FACTOR;

        //apply correction to angle to keep them between 0 and 360 degrees
    	if(angleX > 360) {
    		angleX = angleX - 360;
    	}
    	if(angleX < 0) {
    		angleX = 360 - angleX;
    	}
    	if(angleY > 360) {
    		angleY = angleY - 360;
    	}
    	if(angleY < 0) {
    		angleY = 360 - angleY;
    	}

    	//pass angle to the renderer
    	renderer.angleX = angleX;
    	renderer.angleY = angleY;
    	
    	Log.d(TAG, "X:"+angleX+", Y:"+angleY);
    	
    }
    
    /**
     * Sets the layer.
     *
     * @param layer the new layer
     */
    public void setLayer(int layer) {
    	renderer.setLayer(layer);	
    }

}