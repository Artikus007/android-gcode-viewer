package avb.gcode.opengl11.objects;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import avb.gcode.opengl11.nativeLib.NativeLib;
import avb.gcode.process.Coordinates;
import avb.gcode.process.Coordinates.Type;
import avb.gcode.process.DataStorage;

import javax.microedition.khronos.opengles.GL10;

/**
 * The Class Layer3D.
 * Creates OpenGL data for one layer.
 * The data is extracted from the Data structure 
 * DataStorage.getLayers().get(layer).coordinates
 * and is converted into buffers and passed to OpenGL.
 * 
 * @author Alexander von Birgelen
 */
public class Layer3D {
	
	/** The vertices. */
	private float vertices[];
	/** The order to connect the vertices. */
	private short[] indices;
	/** The colors. */
	private float colors[];
	/** The vertex buffer. */
	private FloatBuffer vertexBuffer;
	/** The index buffer. */
	private ShortBuffer indexBuffer;
	/** The color buffer. */
	private FloatBuffer colorBuffer;
	
	/** The number of indices. */
	private int noIndices = 0;
	
	/**
	 * Instantiates a new layer.
	 *
	 * @param layer The layer
	 * @throws OutOfMemoryError The out of memory error
	 */
	public Layer3D(int layer) throws OutOfMemoryError{
		
		ByteBuffer vbb = null;
		ByteBuffer ibb = null;
		ByteBuffer cbb = null;
		
		NativeLib nl = new NativeLib();
		
		try {
			//create vertices, indices and colors
			processObject(layer);
			
			noIndices = indices.length;
			
			//create buffers:
			// float is 4 bytes so multiply the number of vertices with 4.
			//allocateDirect() suffers from OutOfMemoryError
			//vbb = ByteBuffer.allocateDirect(vertices.length * 4);	
			vbb = nl.allocNativeBuffer(vertices.length * 4);
			vbb.order(ByteOrder.nativeOrder());
			vertexBuffer = vbb.asFloatBuffer();
			vertexBuffer.put(vertices);
			vertexBuffer.position(0);
			vertices = null;
			
			// short is 2 bytes so multiply the number of indices with 2.
			//allocateDirect() suffers from OutOfMemoryError
			//ibb = ByteBuffer.allocateDirect(indices.length * 2);
			ibb = nl.allocNativeBuffer(indices.length * 2);
			ibb.order(ByteOrder.nativeOrder());
			indexBuffer = ibb.asShortBuffer();
			indexBuffer.put(indices);
			indexBuffer.position(0);
			indices = null;
			
			// float is 4 bytes so multiply the number of colors with 4.
			//allocateDirect() suffers from OutOfMemoryError
			//cbb = ByteBuffer.allocateDirect(colors.length * 4);
			cbb = nl.allocNativeBuffer(colors.length * 4);
			cbb.order(ByteOrder.nativeOrder());
			colorBuffer = cbb.asFloatBuffer();
			colorBuffer.put(colors);
			colorBuffer.position(0);
			colors = null;
			
		}catch(OutOfMemoryError e) {
			nl.freeNativeBuffer(vbb);
			nl.freeNativeBuffer(ibb);
			nl.freeNativeBuffer(cbb);
			throw e;	//Throw Exception all the way back to the Activity 
						//to be able to send a Toast.
		}
		
	}
	
	/**
	 * Draw the object.
	 *
	 * @param gl the OpenGL data
	 */
	public void draw(GL10 gl) {
		// Counter-clockwise winding.
		gl.glFrontFace(GL10.GL_CCW);
		// Enabled the vertices buffer and the color array
		gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);

		// Point out the where the color buffer is.
		gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuffer);
		
		//Draw Elements
		gl.glDrawElements(GL10.GL_LINE_STRIP, noIndices, 
				GL10.GL_UNSIGNED_SHORT, indexBuffer);
		
		// Disable the vertices and color buffer.
		gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
		 
	}
	
	/**
	 * Extract data out of the objects
	 *
	 * @param layer the layer
	 * @throws OutOfMemoryError the out of memory error
	 */
	private void processObject(int layer) throws OutOfMemoryError {
		
		//Get number of items for this layer
		int items=DataStorage.getLayers().get(layer).coordinates.size();

		//*3 because X,Y and Z are needed
		this.vertices = new float[items*3];
		
		//*4 because red,green,blue and alpha are needed for every vertice
		this.colors = new float[items*4]; 
		
		int i = 0;
		int j = 0;
		
		for(Coordinates c : DataStorage.getLayers().get(layer).coordinates) {
			
			vertices[i] = (float)c.X;	//X
			i++;
			vertices[i] = (float)c.Y;	//Y
			i++;
			vertices[i] = (float)c.Z;	//Z
			i++;
			
			float red,green,blue,alpha;
			red=green=blue=0;
			alpha = 1;
			
			if(c.type == Type.UNKNOWN) {
				red = 0.5f;
				green = 0.5f;
				blue = 0.5f;
            }
            if(c.type == Type.MOVE) {
            	red = green = blue = 0.5f;
            	alpha = 0.2f;
            }
            if(c.type == Type.SKIRT) {
            	blue = 1.0f;
            }
            if(c.type == Type.PERIMETER) {
            	red = 1.0f;
            }
            if(c.type == Type.FILL) {
            	green = 1.0f;
            }
			
	        colors[j] = red;	//RED
			colors[j+1] = green;//GREEN
			colors[j+2] = blue;	//BLUE
			colors[j+3] = alpha;//ALPHA
			
			j=j+4;
			
		}
		
	
		this.indices = new short[vertices.length/3];
		for(i=0;i<this.indices.length;i++) {
			//OpenGL ES only supports short buffer for indices. 
			//This is a correction for unsigned short which is not supported 
			//in java by default.
			if(i > 32767) {
				this.indices[i] = (short) (i - 65536);
			}else{
				this.indices[i] = (short)i;
			}
		}

	}
	
}
