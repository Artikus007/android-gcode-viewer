package avb.gcode.opengl11.objects;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import javax.microedition.khronos.opengles.GL10;
import avb.gcode.opengl11.nativeLib.NativeLib;

/**
 * This class is used do display float numbers in XY orientation.
 * Every letter consists of 10 vertices. This works like a 7 segment display.
 * The additional segments are used do display the decimal separator and the 
 * minus.
 * 
 * Supported chars: 0123456789.-
 * 
 * @author Alexander von Birgelen
 *
 */
public class NumberXY {

	/** The width of a letter. */
	public static final float LETTER_WIDTH = 1.5f;
	
	/** The height of a letter. */
	public static final float LETTER_HEIGHT = 3.0f;
	
	/** The space between letters. */
	public static final float LETTER_SPACING = 1.0f;
	
	/** The number of vertices for each letter. */
	private static final int VERTICES_PER_LETTER = 10;
	
	/** The vertices. */
	private float vertices[];
	/** The order to connect the vertices. */
	private short[] indices;
	/** The colors. */
	private float colors[];
	/** The vertex buffer. */
	private FloatBuffer vertexBuffer;
	/** The index buffer. */
	private ShortBuffer indexBuffer;
	/** The color buffer. */
	private FloatBuffer colorBuffer;
	
	/** The number of indices. */
	private int noIndices = 0;
	
	//every letter consists of 7 vertices
	//	4		5
	//	2,8	9	3
	//	0	7	1
	//		6
	
	/**
	 * Creates a new Text in XY plane at the given coordinates.
	 * Supported chars (for numbers only) : 0123456789.-
	 *
	 * @param txt the Text to render
	 * @param x the x position
	 * @param y the y position
	 * @param z the z position
	 */
	public NumberXY(String txt, float x, float y, float z) {
		
		ByteBuffer vbb = null;
		ByteBuffer ibb = null;
		ByteBuffer cbb = null;
		
		NativeLib nl = new NativeLib();
		
		vertices = new float[txt.length() * VERTICES_PER_LETTER * 3];
		colors = new float[txt.length() * VERTICES_PER_LETTER * 4];
		indices = new short[getIndiceCount(txt)];
		
		float xOffset = x;
		float yOffset = y;
		float zOffset = z;
		int v_count = 0;
		int i_count = 0;
		
		for(int j=0; j<txt.length(); j++) {
			
			float[] temp_v = getVertices(xOffset,yOffset,zOffset);
			short[] temp_i = getIndices(txt.charAt(j), (short)(v_count/3));

			for(int i=0;i<temp_v.length;i++) {
				vertices[v_count] = temp_v[i];
				v_count++;
			}
			
			for(int i=0;i<temp_i.length;i++) {
				indices[i_count] = temp_i[i];
				i_count++;
			}
			
			if(txt.charAt(j) == '.' || txt.charAt(j) == '-') {
				xOffset += LETTER_SPACING;
			}else{
				xOffset += LETTER_WIDTH + LETTER_SPACING;
			}
			
		}

		for(int j=0;j<colors.length;j=j+4) {
			colors[j] = 1.0f;	//RED
			colors[j+1] = 1.0f;//GREEN
			colors[j+2] = 1.0f;	//BLUE
			colors[j+3] = 1.0f;//ALPHA
		}
		
		noIndices = indices.length;
		
		//create buffers:
		// float is 4 bytes so multiply the number of vertices with 4.
		vbb = nl.allocNativeBuffer(vertices.length * 4);
		vbb.order(ByteOrder.nativeOrder());
		vertexBuffer = vbb.asFloatBuffer();
		vertexBuffer.put(vertices);
		vertexBuffer.position(0);
		vertices = null;
		
		// short is 2 bytes so multiply the number of indices with 2.
		ibb = nl.allocNativeBuffer(indices.length * 2);
		ibb.order(ByteOrder.nativeOrder());
		indexBuffer = ibb.asShortBuffer();
		indexBuffer.put(indices);
		indexBuffer.position(0);
		indices = null;

		// float is 4 bytes so multiply the number of colors with 4.
		cbb = nl.allocNativeBuffer(colors.length * 4);
		cbb.order(ByteOrder.nativeOrder());
		colorBuffer = cbb.asFloatBuffer();
		colorBuffer.put(colors);
		colorBuffer.position(0);
		colors = null;

		
	}
	
	/**
	 * Draw the object.
	 *
	 * @param gl the OpenGL data
	 */
	public void draw(GL10 gl) {
		// Counter-clockwise winding.
				gl.glFrontFace(GL10.GL_CCW);
				// Enabled the vertices buffer and the color array
				gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
				// Specifies the location and data format of an array of vertex
				// coordinates to use when rendering.
				gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);

				// Point out the where the color buffer is.
				gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuffer);
				
				//Draw Elements
				gl.glDrawElements(GL10.GL_LINES, noIndices, 
						GL10.GL_UNSIGNED_SHORT, indexBuffer);

				// Disable the vertices and color buffer.
				gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
	}

	/**
	 * Gets the vertices for a char.
	 *
	 * @param xOffset the x offset
	 * @param yOffset the y offset
	 * @param zOffset the z offset
	 * @return the vertices
	 */
	public float[] getVertices(float xOffset, float yOffset, float zOffset){
			float[] vertices;
			vertices = new float[VERTICES_PER_LETTER*3];
			
			vertices[0] = xOffset;//x
			vertices[1] = yOffset;//y
			vertices[2] = zOffset;//z
			
			vertices[3] = xOffset + LETTER_WIDTH;//x
			vertices[4] = yOffset;//y
			vertices[5] = zOffset;//z
			
			vertices[6] = xOffset;//x
			vertices[7] = yOffset + LETTER_HEIGHT/2.0f;//y
			vertices[8] = zOffset;//z
			
			vertices[9] = xOffset + LETTER_WIDTH;//x
			vertices[10] = yOffset + LETTER_HEIGHT/2.0f;//y
			vertices[11] = zOffset;//z
			
			vertices[12] = xOffset;//x
			vertices[13] = yOffset + LETTER_HEIGHT;//y
			vertices[14] = zOffset;//z
			
			vertices[15] = xOffset + LETTER_WIDTH;//x
			vertices[16] = yOffset + LETTER_HEIGHT;//y
			vertices[17] = zOffset;//z
			
			//for decimal point:
			vertices[18] = xOffset + LETTER_WIDTH/2.0f;//x
			vertices[19] = yOffset + LETTER_HEIGHT/10.0f;//y
			vertices[20] = zOffset;//z
			
			vertices[21] = xOffset + LETTER_WIDTH/2.0f; //x
			vertices[22] = yOffset - LETTER_HEIGHT/10.0f;//y
			vertices[23] = zOffset;//z
			
			//for minus:
			vertices[24] = xOffset;//x
			vertices[25] = yOffset + LETTER_HEIGHT/2.0f;//y
			vertices[26] = zOffset;//z
			
			vertices[27] = xOffset + LETTER_WIDTH-1.0f;//x
			vertices[28] = yOffset + LETTER_HEIGHT/2.0f;//y
			vertices[29] = zOffset;//z
			
			return vertices;
	}
	
	/**
	 * Connect indices to display a specific char.
	 *
	 * @param S the char
	 * @param verticeOffset the vertice offset for connecting multiple chars
	 * @return the indices
	 */
	public short[] getIndices(char S, short verticeOffset) {
		short[] indices;
		
		switch(S) {
		
		case '0':
			indices = new short[4*2];
			indices[0] = 0;
			indices[1] = 1;
			
			indices[2] = 1;
			indices[3] = 5;
			
			indices[4] = 5;
			indices[5] = 4;
			
			indices[6] = 4;
			indices[7] = 0;
			
			break;
			
		case '1':
			indices = new short[1*2];
			indices[0] = 1;
			indices[1] = 5;
			
			break;
			
		case '2':
			indices = new short[5*2];
			indices[0] = 4;
			indices[1] = 5;
			
			indices[2] = 5;
			indices[3] = 3;
			
			indices[4] = 3;
			indices[5] = 2;
			
			indices[6] = 2;
			indices[7] = 0;
			
			indices[8] = 0;
			indices[9] = 1;
			
			break;
			
		case '3':
			indices = new short[5*2];
			indices[0] = 0;
			indices[1] = 1;
			
			indices[2] = 1;
			indices[3] = 3;
			
			indices[4] = 2;
			indices[5] = 3;
			
			indices[6] = 3;
			indices[7] = 5;
			
			indices[8] = 5;
			indices[9] = 4;
			
			break;
			
		case '4':
			indices = new short[3*2];
			indices[0] = 4;
			indices[1] = 2;
			
			indices[2] = 2;
			indices[3] = 3;
			
			indices[4] = 5;
			indices[5] = 1;
			
			break;
			
		case '5':
			indices = new short[5*2];
			indices[0] = 0;
			indices[1] = 1;
			
			indices[2] = 1;
			indices[3] = 3;
			
			indices[4] = 3;
			indices[5] = 2;
			
			indices[6] = 2;
			indices[7] = 4;
			
			indices[8] = 4;
			indices[9] = 5;
			
			break;
			
		case '6':
			indices = new short[5*2];
			indices[0] = 0;
			indices[1] = 1;
			
			indices[2] = 1;
			indices[3] = 3;
			
			indices[4] = 3;
			indices[5] = 2;
			
			indices[6] = 0;
			indices[7] = 4;
			
			indices[8] = 4;
			indices[9] = 5;
			
			break;
			
		case '7':
			indices = new short[2*2];
			indices[0] = 1;
			indices[1] = 5;
			
			indices[2] = 5;
			indices[3] = 4;
			
			break;
			
		case '8':
			indices = new short[5*2];
			indices[0] = 0;
			indices[1] = 4;
			
			indices[2] = 1;
			indices[3] = 5;
			
			indices[4] = 0;
			indices[5] = 1;
			
			indices[6] = 2;
			indices[7] = 3;
			
			indices[8] = 4;
			indices[9] = 5;

			break;
			
		case '9':
			indices = new short[5*2];
			indices[0] = 0;
			indices[1] = 1;
			
			indices[2] = 1;
			indices[3] = 5;
			
			indices[4] = 3;
			indices[5] = 2;
			
			indices[6] = 2;
			indices[7] = 4;
			
			indices[8] = 4;
			indices[9] = 5;
			
			break;
			
		case '.':
			indices = new short[1*2];
			indices[0] = 6;
			indices[1] = 7;
			break;
			
		case '-':
			indices = new short[1*2];
			indices[0] = 8;
			indices[1] = 9;
			break;
			
		default:
			indices = null;
		}
		
		for(int i=0;i<indices.length;i++) {
			indices[i] += verticeOffset;
		}
		
		return indices;
	}
	
	/**
	 * Gets the numbers of indices for the complete text.
	 *
	 * @param txt the text
	 * @return the indice count
	 */
	private int getIndiceCount(String txt) {
		
		int count = 0;
		
		for(int i=0; i<txt.length(); i++) {
			switch(txt.charAt(i)) {
			
			case '0':
				count += 4;
				break;
				
			case '1':
				count += 1;
				break;
				
			case '2':
				count += 5;
				break;
				
			case '3':
				count += 5;
				break;
				
			case '4':
				count += 3;
				break;
				
			case '5':
				count += 5;
				break;
				
			case '6':
				count += 5;
				break;
				
			case '7':
				count += 2;
				break;
				
			case '8':
				count += 5;
				break;
				
			case '9':
				count += 5;
				break;
				
			case '.':
				count += 1;
				break;
				
			case '-':
				count += 1;
				break;
				
			default:
				indices = null;
			}
		}
		
		count = count * 2;
		
		return count;
	}
	
}
