package avb.gcode;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import avb.gcode.R;
import avb.gcode.process.ProcessGCode;


/**
 * The Class GCodeViewer is used for file selection.
 *
 * @author Alexander von Birgelen
 */
public class GCodeViewer extends ListActivity {
    
    /** The current directory. */
    private File currentDir;
    
    /** The file adapter. */
    private FileAdapter adapter;
    
    /** Menu option - Show all file types or just specific gcode files. */
    private boolean showallfiles = false;	
    
    /** Menu option - Show hidden files or hide hidden files. */
    private boolean showhidden = false;
    
    /** 
     * onCreate is called when the Activity is first created.
     * @param savedInstanceState The saved instance state.
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentDir = new File("/sdcard/");	//Start directory is sd card.
        showContents(currentDir);
    }
    
    /**
     * Show contents of the current directory.
     *
     * @param f the current directory
     */
    private void showContents(File f)
    {
    	//get directory content and prepare data for view
         File[]contents = f.listFiles();
         this.setTitle("Current Dir: "+f.getName());
         List<FileItem>directories = new ArrayList<FileItem>();
         List<FileItem>files = new ArrayList<FileItem>();
         try{
             for(File ff: contents)
             {
            	if(!ff.getName().startsWith(".") || showhidden) {
	                if(ff.isDirectory()) {
	                    directories.add(new FileItem(ff));
	                }else{
	                	String name = ff.getName();
	                	if(name.endsWith(".gcode") || name.endsWith(".ngc") 
	                	|| name.endsWith(".txt") || showallfiles) {
	                		files.add(new FileItem(ff));
	                	}
	                }
            	}
             }
         }catch(Exception e){
            e.printStackTrace();
         }
         Collections.sort(directories);
         Collections.sort(files);
         
         //Add all Contents to directories, only directories before files, 
         //no additional list required
         directories.addAll(files);	
         
         if(!f.getName().equalsIgnoreCase("sdcard"))
             directories.add(0,new FileItem(f.getParentFile(),true));
         
         adapter = new FileAdapter(GCodeViewer.this,R.layout.file_view,directories);
         this.setListAdapter(adapter);
         
    }

    /**
     * Called when a list item is clicked.
     * @param l The list view
     * @param v The view
     * @param position The position
     * @param id The ID
     * @see android.app.ListActivity#onListItemClick(android.widget.ListView, android.view.View, int, long)
     */
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
   
    	//Change to new directory or select file
    	
        super.onListItemClick(l, v, position, id);
        FileItem f = adapter.getItem(position);
        if(f.isDirectory()){
                currentDir = new File(f.getPath());
                showContents(currentDir);
        }else{
            selectFile(f);
        }
    }
    
    /**
     * Select a file and start the next Activity.
     *
     * @param f the file
     */
    private void selectFile(FileItem f) {
        Toast.makeText(this, "Loading "+f.getName(), Toast.LENGTH_LONG).show();
        
        Intent intent = new Intent(this,ProcessGCode.class);
        intent.setAction("gcodeviewer");
        intent.putExtra("File", f.getPath());
        startActivity(intent);
        
    }
    
    /**
     * Create an option menu.
     * @param menu The option Menu
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.file_menu, menu);
    	return true;
    }
    
    /**
     * Called when an option menu item is selected.	
     * @param item The menu item.
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    public boolean onOptionsItemSelected(MenuItem item) {
    	
    	//This handles the menu options
    	
    	if(item.getItemId() == R.id.showallfiles ) {
    		showallfiles = !showallfiles;
    		if(showallfiles) {
    			item.setTitle("G-Code files only");
    		}else{
    			item.setTitle("Show all file types");
    		}
    		showContents(currentDir);
    		return true;
    	}else if(item.getItemId() == R.id.showhidden) {
    		showhidden = !showhidden;
    		if(showhidden) {
    			item.setTitle("Normal files only");
    		}else{
    			item.setTitle("Show hidden files");
    		}
    		showContents(currentDir);
    		return true;
    	}else{
    		return super.onOptionsItemSelected(item);
    	}
    }
    
}