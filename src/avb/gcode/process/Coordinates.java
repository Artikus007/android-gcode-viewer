package avb.gcode.process;

/**
 * The Class Coordinates.
 * @author Alexander von Birgelen
 */
public class Coordinates{
	
	/** The layer. */
	public int layer = 0;
    
    /** The line number. */
    public int line;
    
    /** The type. */
    public Type type;
    
    /** The X axis. */
    public float X = 0;
    
    /** The Y axis. */
    public float Y = 0;
    
    /** The Z axis. */
    public float Z = 0;
    
    ///** The Extrusion axis */
    //public float A = 0;
    
    /** The Feed rate. */
    public float F = 0;
    
    /**
     * The Enum Type.
     */
    public enum Type {
    	
	    /** unknown when verbose gcode is disabled. */
	    UNKNOWN,
		
		/** outer skirt. */
		SKIRT,
		
		/** retract plastic. */
		RETRACT,
		
		/** perimeter shells. */
		PERIMETER,
		
		/** retract compensation. */
		COMPENSATE,
		/** solid infill. */
		FILL,
		
		/** movement without extrusion. */
		MOVE,
		
		/** bridging. */
		BRIDGE
    }
    
}
