package avb.gcode.process;


import java.util.ArrayList;

import android.content.Context;

/**
 * The Class DataStorage contains information that must be available in
 * many Activities.
 * @author Alexander von Birgelen
 */
public class DataStorage {
	
	/** The layers. */
	private static ArrayList<Layer> layers;
	
	/** Marker for Memory Exception. */
	private static boolean memoryexception = false;
	
	/** The minimal x. */
	private static float minimalX;
	
	/** The maximal x. */
	private static float maximalX;
	
	/** The minimal y. */
	private static float minimalY;
	
	/** The maximal y. */
	private static float maximalY;
	
	/** The minimal z. */
	private static float minimalZ;
	
	/** The maximal z. */
	private static float maximalZ;
	
	/** The context_3d. */
	private static Context context_3d;
	
	/**
	 * Get the exception state.
	 *
	 * @return true or false
	 */
	public static boolean isMemoryexception() {
		return memoryexception;
	}

	/**
	 * Set the value for exception.
	 *
	 * @param memoryexception the new memoryexception
	 */
	public static void setMemoryexception(boolean memoryexception) {
		DataStorage.memoryexception = memoryexception;
	}

	/**
	 * Put layers.
	 *
	 * @param l the l
	 */
	public static void putLayers(ArrayList<Layer> l) {
		layers = l;
	}
	
	/**
	 * Gets the layers.
	 *
	 * @return the layers
	 */
	public static ArrayList<Layer> getLayers() {
		return layers;
	}

	/**
	 * Gets the minimal x.
	 *
	 * @return the minimal x
	 */
	public static float getMinimalX() {
		return minimalX;
	}

	/**
	 * Sets the minimal x.
	 *
	 * @param minimalX the new minimal x
	 */
	public static void setMinimalX(float minimalX) {
		DataStorage.minimalX = minimalX;
	}


	/**
	 * Gets the minimal y.
	 *
	 * @return the minimal y
	 */
	public static float getMinimalY() {
		return minimalY;
	}

	/**
	 * Sets the minimal y.
	 *
	 * @param minimalY the new minimal y
	 */
	public static void setMinimalY(float minimalY) {
		DataStorage.minimalY = minimalY;
	}

	/**
	 * Gets the minimal z.
	 *
	 * @return the minimal z
	 */
	public static float getMinimalZ() {
		return minimalZ;
	}

	/**
	 * Sets the minimal z.
	 *
	 * @param minimalZ the new minimal z
	 */
	public static void setMinimalZ(float minimalZ) {
		DataStorage.minimalZ = minimalZ;
	}

	/**
	 * Gets the maximal x.
	 *
	 * @return the maximal x
	 */
	public static float getMaximalX() {
		return maximalX;
	}

	/**
	 * Sets the maximal x.
	 *
	 * @param maximalX the new maximal x
	 */
	public static void setMaximalX(float maximalX) {
		DataStorage.maximalX = maximalX;
	}

	/**
	 * Gets the maximal y.
	 *
	 * @return the maximal y
	 */
	public static float getMaximalY() {
		return maximalY;
	}

	/**
	 * Sets the maximal y.
	 *
	 * @param maximalY the new maximal y
	 */
	public static void setMaximalY(float maximalY) {
		DataStorage.maximalY = maximalY;
	}

	/**
	 * Gets the maximal z.
	 *
	 * @return the maximal z
	 */
	public static float getMaximalZ() {
		return maximalZ;
	}

	/**
	 * Sets the maximal z.
	 *
	 * @param maximalZ the new maximal z
	 */
	public static void setMaximalZ(float maximalZ) {
		DataStorage.maximalZ = maximalZ;
	}

	/**
	 * Gets the context_3d.
	 *
	 * @return the context_3d
	 */
	public static Context getContext_3d() {
		return context_3d;
	}

	/**
	 * Sets the context_3d.
	 *
	 * @param context_3d the new context_3d
	 */
	public static void setContext_3d(Context context_3d) {
		DataStorage.context_3d = context_3d;
	}
	
	
}
