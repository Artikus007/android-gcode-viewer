package avb.gcode.process;

import java.util.ArrayList;

/**
 * The Class Layer.
 * @author Alexander von Birgelen
 */
public class Layer {
	
	/** The coordinates. */
	public ArrayList<Coordinates> coordinates;
	
	/**
	 * Instantiates a new layer.
	 */
	public Layer() {
		coordinates = new ArrayList<Coordinates>();
	}
}
