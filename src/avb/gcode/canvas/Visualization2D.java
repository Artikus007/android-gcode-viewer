package avb.gcode.canvas;

import avb.gcode.R;
import avb.gcode.process.DataStorage;
import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;

/**
 * The Class Visualization2D.
 * @author Alexander von Birgelen
 */
public class Visualization2D extends Activity implements OnClickListener,
							  								 OnTouchListener {
	
	/** The maximum zoom factor. */
	private final float MAX_ZOOM_FACTOR = 8.0f;
	
	/** The Layer to paint. */
	private Layer2D paintlayer;
	
	/** The button to go up one layer. */
	private Button upperlayer;
	
	/** The button to go down one layer. */
	private Button lowerlayer;
	
	/** The text information to show layer information. */
	private TextView layers;
	
	/** The maximum layer. */
	private int maxlayer;
	
	/** The active layer. */
	private int activelayer;
	
	/** The delta x for painting. */
	private float deltax;
	
	/** The delta y for painting. */
	private float deltay;
	
	/** The finger count for multi touch gestures. */
	private int fingers = 0;
    
    /** The previous x. */
    private float previousX;
    
    /** The previous y. */
    private float previousY;
    
    /** The scale factor. */
    private float scaleFactor = 1.0f;
    
    /** The old distance. */
    private float oldDistance;
    
    /** The new distance. */
    private float newDistance;
    
    /** Move jump bugfix */
    private boolean block = false;
    
	/**
	 * Called when Activity is created.
	 * @param savedInstanceState The instance state
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        	//get delta values from ProcessGCode Activity
        	deltax = DataStorage.getMaximalX() - DataStorage.getMinimalX();
        	deltay = DataStorage.getMaximalY() - DataStorage.getMinimalY();
        	
        	//get maximum layer
        	maxlayer = DataStorage.getLayers().size()-1;
        	activelayer = 1;
        	
        	//init views
        	setContentView(R.layout.visu2d);
        	
        	paintlayer = (Layer2D) findViewById(R.id.paintlayer);
        	paintlayer.setOnTouchListener(this);
        	
        	upperlayer = (Button) findViewById(R.id.upperlayer);
        	upperlayer.setOnClickListener(this);
        	lowerlayer = (Button) findViewById(R.id.lowerlayer);
        	lowerlayer.setOnClickListener(this);
        	layers = (TextView) findViewById(R.id.layers);
        	
        	layers.setText(activelayer+"/"+maxlayer);

    }

	/**
	 * Called when a click is performed.
	 * @param arg0 The view element on which the action was performed
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	public void onClick(View arg0) {
		
		if(arg0 == lowerlayer) {
			//move down one layer
			if(activelayer > 1) {
				activelayer--;
			}else{
				activelayer = 1;
			}
		}
		if(arg0 == upperlayer) {
			//move up one layer
			if(activelayer < maxlayer) {
				activelayer++;
			}else{
				activelayer = maxlayer;
			}
			
		}
		
		paintlayer.setLayer(activelayer);	//set layer
		layers.setText(activelayer+"/"+maxlayer);
		
	}

	/**
	 * Called when the focus changes (start of activity or rotation of phone)
	 * @param hasFocus Window has focus or not.
	 * @see android.app.Activity#onWindowFocusChanged(boolean)
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		
		//default scaling and translation:
    	paintlayer.translateX = ((float)paintlayer.getWidth())/2.0f;
    	paintlayer.translateY = ((float)paintlayer.getHeight())/2.0f;
    	
    	//use only one scaling factor for symmetry
    	if(deltax > 0 && deltay > 0) {
        	if(deltax > deltay) {
        		scaleFactor = ((float)paintlayer.getWidth()) / deltax;
        	}else{
        		scaleFactor = ((float)paintlayer.getHeight()) / deltay;
        	}
    	}else{
    		scaleFactor = 1.0f;
    	}
    	
    	paintlayer.scaleFactor = scaleFactor;
    	paintlayer.setLayer(1);
		
	}
	
	/**
	 * Gets the finger distance.
	 *
	 * @param event the event
	 * @return the finger distance
	 */
	private float getFingerDistance(MotionEvent event) {
		   float x = event.getX(0) - event.getX(1);
		   float y = event.getY(0) - event.getY(1);
		   return FloatMath.sqrt(x * x + y * y);
	}

	/**
	 * This listener implements the multitouch commands.
	 * @param v The view
	 * @param e The motion event
	 */
	public boolean onTouch(View v, MotionEvent e) {
		
		//no touch events for buttons
		if(v == lowerlayer || v == upperlayer) {
			return true;
		}
		
		//get finger coordinates
		float x = e.getX();
        float y = e.getY();
        
        switch (e.getAction() & MotionEvent.ACTION_MASK) {	//get action
        
	        case MotionEvent.ACTION_MOVE:
	        	
	        	//calculate move distance
	        	float dx = x - previousX;
	            float dy = y - previousY;
	        	
	            if(!block) {
		        	if(fingers == 1) {
		        		//TRANSLATE when one finger used
		        		paintlayer.translateX += dx;
		        		paintlayer.translateY += dy;
		        	}
		            if(fingers == 2) {
		            	//ZOOM when two fingers used
		            	newDistance = getFingerDistance(e);
		        		float factor = newDistance/oldDistance;
		        		if(factor > MAX_ZOOM_FACTOR) {
		        			factor = MAX_ZOOM_FACTOR;
		        		}
		        		scaleFactor *= factor;	//multiply scaling
		        		paintlayer.scaleFactor = scaleFactor;
		        		
		        		oldDistance = newDistance;
		            }
	            }
	            break;
	        case MotionEvent.ACTION_DOWN:
	        	fingers = 1;	//translate
	        	break;
	        case MotionEvent.ACTION_UP:
	        	fingers = 0;
	        	block = false;
	        	break;
	        case MotionEvent.ACTION_POINTER_DOWN:
	        	fingers = 2;	//zoom
	        	oldDistance = getFingerDistance(e); //set old distance
	        	break;
	        case MotionEvent.ACTION_POINTER_UP:
	        	fingers = 1;
	        	block = true;
	        	break;
        	
        }
        previousX = x;
        previousY = y;
        
        paintlayer.invalidate();	//request repaint
		
		return true;
	
	}
        
}