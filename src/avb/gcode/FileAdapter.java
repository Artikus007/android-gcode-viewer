package avb.gcode;

import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import avb.gcode.R;

/**
 * The Class FileAdapter inflates the layout with the directory data
 * @author Alexander von Birgelen
 */
public class FileAdapter extends ArrayAdapter<FileItem>{

    /** The context. */
    private Context c;
    
    /** The id. */
    private int id;
    
    /** The items. */
    private List<FileItem>items;
    
    /**
     * Instantiates a new file adapter.
     *
     * @param context the context
     * @param textViewResourceId the text view resource id
     * @param objects the objects
     */
    public FileAdapter(Context context, int textViewResourceId,
    		List<FileItem> objects) {
        super(context, textViewResourceId, objects);
        c = context;
        id = textViewResourceId;
        items = objects;
    }
    
    /** Return the item
     * @param i The index of the item.
     * @see android.widget.ArrayAdapter#getItem(int)
     */
    public FileItem getItem(int i)
    {
    	return items.get(i);
    }
    
    /** Return the inflated view.
     * @param position The position
     * @param convertView The view to inflate
     * @param parent The parent group
     * @see android.widget.ArrayAdapter#getView(int, android.view.View, 
     * android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
           View v = convertView;
           if (v == null) {
        	   //Create inflater
               LayoutInflater vi = (LayoutInflater)c.getSystemService(
            		   				Context.LAYOUT_INFLATER_SERVICE);
               v = vi.inflate(id, null);
           }
           final FileItem f = items.get(position);
           //Set item data
           if (f != null) {
                   TextView t1 = (TextView) v.findViewById(R.id.TextView01);
                   TextView t2 = (TextView) v.findViewById(R.id.TextView02);
                   
                   if(t1!=null)
                       t1.setText(f.getName());
                   if(t2!=null)
                       t2.setText(f.getData());
           }
           return v;
    }

}

